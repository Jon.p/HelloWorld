public class SayHelloTo2 {

	private String name = "";

	public SayHelloTo2(String myName) {
		this.name = myName;
	}

	public String helloTo2() {
		return "Hello22 " + this.name + "!";
	}
}
