public class SayByeTo {

	private String name = "";

	public SayByeTo(String myName) {
		this.name = myName;
	}

	public String byeTo() {
		return "Bye " + this.name + "!";
	}
}
